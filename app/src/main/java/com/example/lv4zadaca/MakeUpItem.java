package com.example.lv4zadaca;

public class MakeUpItem {
    private int id;
    private String brand;
    private String name;
    private String price;
    private String image_link;
    private String description;
    private String rating;


    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getImage_link() {
        return image_link;
    }

    public String getDescription() {
        return description;
    }

    public String getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "ID" + this.id;
    }
}
