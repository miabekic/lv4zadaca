package com.example.lv4zadaca;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("products.json")
    Call<List<MakeUpItem>> getProduct(
            @Query(value = "brand") String brand
    );
}
