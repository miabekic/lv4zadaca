package com.example.lv4zadaca;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class MakeUpItemViewHolder extends RecyclerView.ViewHolder {
    private ImageView imV;
    private TextView tVData, tVDescription;
    public MakeUpItemViewHolder(@NonNull View itemView) {
        super(itemView);
        imV=itemView.findViewById(R.id.imV);
        tVData=itemView.findViewById(R.id.tVData);
        tVDescription=itemView.findViewById(R.id.tVDescription);
    }

    public void setImage(String imageUrl) {
        Picasso.get().load(imageUrl).into(imV);
    }

    public void setData(String data) {
        tVData.setText(data);
    }

    public void setDescription(String description) {

        tVDescription.setText(description.toString().replace("\n",  " "));

    }

}
