package com.example.lv4zadaca;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<MakeUpItemViewHolder> {

    private List<MakeUpItem> dataList = new ArrayList<>();

    @NonNull
    @Override
    public MakeUpItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cell = LayoutInflater.from(parent.getContext()).inflate(R.layout.makeupitem, parent, false);
        return new MakeUpItemViewHolder(cell);
    }

    @Override
    public void onBindViewHolder(@NonNull MakeUpItemViewHolder viewHolder, int position) {
        MakeUpItem item = dataList.get(position);
        viewHolder.setImage(item.getImage_link());
        viewHolder.setData("Name: "+item.getName()+"\nPrice: "+item.getPrice()+"\nRating: "+ (item.getRating()==null ? "" : item.getRating()));
        viewHolder.setDescription(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<MakeUpItem> items) {
        this.dataList.addAll(items);
        notifyDataSetChanged();
    }
}
