package com.example.lv4zadaca;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText eTBrand;
    private Button btnSearch;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private Call<List<MakeUpItem>> apiCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setUpRecycler();
    }

    private void init(){
        eTBrand=findViewById(R.id.eTbrand);
        btnSearch=findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
    }

    private void setUpRecycler(){
        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void setUpApiCall(String brand){
        apiCall = NetworkUtils.getApiInterface().getProduct(brand);

        apiCall.enqueue(new Callback<List<MakeUpItem>>() {
            @Override
            public void onResponse(Call<List<MakeUpItem>> call, Response<List<MakeUpItem>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().size() == 0) {
                        Toast.makeText(MainActivity.this,"Data not found!" ,Toast.LENGTH_LONG).show();
                    }
                    else {
                        adapter.addItems(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<MakeUpItem>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Api call not available", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
            String brand = eTBrand.getText().toString();

            if (brand.isEmpty()) {
                Toast.makeText(MainActivity.this, "You didn't enter the brand!", Toast.LENGTH_SHORT).show();
            }
            else {
                setUpApiCall(brand);
            }
    }
}